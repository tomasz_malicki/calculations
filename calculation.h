#ifndef CALCULATION_H
#define CALCULATION_H

#include <QString>
#include <QDebug>

class calculation
{
public:
    calculation() {};

    template<typename T>
    T sum(T x, T y) { return x + y; };

    template<typename T>
    T substr(T x, T y) { return x - y; };

    template<typename T>
    T multiply(T x, T y) { return x * y;};

    template<typename T>
    T divide(T x, T y)
    {
        try
        {
            if(y == 0) throw QString("Division by zero condition!");
            else return x/y;
        }
        catch(QString msg)
        {
            qDebug() << msg;
            return -1;
        }
    };
};

#endif // CALCULATION_H
