#ifndef TST_HANDLESPOSITIVEINPUT_H
#define TST_HANDLESPOSITIVEINPUT_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include "../calculation.h"
//#include "../calculation.cpp"

using namespace testing;

TEST(SumTestInt, HandlesPositiveInput)
{
    calculation* testObj = new calculation;;

    EXPECT_EQ(testObj->sum<int>(2,7), 9);
    EXPECT_EQ(testObj->sum<int>(25,71), 96);
    EXPECT_EQ(testObj->sum<int>(175,4050), 4225);

}

TEST(SumTestInt, HandlesNegativeInput)
{
    calculation* testObject = new calculation;

    EXPECT_EQ(testObject->sum<int>(-2,-7), -9);
    EXPECT_EQ(testObject->sum<int>(-25,-71), -96);
    EXPECT_EQ(testObject->sum<int>(-175,-4050), -4225);
    EXPECT_EQ(testObject->sum<int>(-175,405), 230);
    EXPECT_EQ(testObject->sum<int>(175,-4050), -3875);
    EXPECT_EQ(testObject->sum<int>(-175, 40), -135);
}

TEST(SumTestInt, HandlesZeroInput)
{
    calculation* testObject = new calculation;;

    EXPECT_EQ(testObject->sum<int>(0, 0), 0);
    EXPECT_EQ(testObject->sum<int>(0,-71), -71);
    EXPECT_EQ(testObject->sum<int>(-175,0), -175);
    EXPECT_EQ(testObject->sum<int>(0,405), 405);
    EXPECT_EQ(testObject->sum<int>(175,0), 175);
}

TEST(SumTestDouble, HandlesPositiveInput)
{
    calculation testObj;

    EXPECT_EQ(testObj.sum<int>(2.0,7.0), 9.0);
    EXPECT_EQ(testObj.sum<int>(25.0,71.0), 96.0);
    EXPECT_EQ(testObj.sum<int>(175.0,4050.0), 4225.0);

}

TEST(SumTestDouble, HandlesNegativeInput)
{
    calculation testObject;;

    EXPECT_EQ(testObject.sum<int>(-2.0,-7.0), -9.0);
    EXPECT_EQ(testObject.sum<int>(-25.0,-71.0), -96.0);
    EXPECT_EQ(testObject.sum<int>(-175.0,-4050.0), -4225.0);
    EXPECT_EQ(testObject.sum<int>(-175.0,405.0), 230.0);
    EXPECT_EQ(testObject.sum<int>(175.0,-4050.0), -3875.0);
    EXPECT_EQ(testObject.sum<int>(-175.0, 40.0), -135.0);
}

TEST(SumTestDouble, HandlesZeroInput)
{
    calculation testObject;

    EXPECT_EQ(testObject.sum<int>(0.0, 0.0), 0.0);
    EXPECT_EQ(testObject.sum<int>(0.0,-71.0), -71.0);
    EXPECT_EQ(testObject.sum<int>(-175.0,0.0), -175.0);
    EXPECT_EQ(testObject.sum<int>(0.0,405.0), 405.0);
    EXPECT_EQ(testObject.sum<int>(175.0,0.0), 175.0);
}

TEST(DivideTestInt, HandlesPositiveInput)
{
    calculation testObj;

    EXPECT_EQ(testObj.divide<int>(2,7), 0);
    EXPECT_EQ(testObj.divide<int>(25,71), 0);
    EXPECT_EQ(testObj.divide<int>(175,4050), 0);

}

TEST(DivideTestInt, HandlesNegativeInput)
{
    calculation testObject;;

    EXPECT_EQ(testObject.divide<int>(-2,-7), 0);
    EXPECT_EQ(testObject.divide<int>(-25,-71), 0);
    EXPECT_EQ(testObject.divide<int>(-175,-4050), 0);
    EXPECT_EQ(testObject.divide<int>(-175,405), 0);
    EXPECT_EQ(testObject.divide<int>(175,-4050), 0);
    EXPECT_EQ(testObject.divide<int>(-175, 40), -4);
}

TEST(DivideTestInt, HandlesZeroInput)
{
    calculation testObject;

    EXPECT_EQ(testObject.divide<int>(0, 0), -1);
    EXPECT_EQ(testObject.divide<int>(0,-71), 0);
    EXPECT_EQ(testObject.divide<int>(-175,0), -1);
    EXPECT_EQ(testObject.divide<int>(0,405), 0);
    EXPECT_EQ(testObject.divide<int>(175,0), -1);
}

#endif // TST_HANDLESPOSITIVEINPUT_H
